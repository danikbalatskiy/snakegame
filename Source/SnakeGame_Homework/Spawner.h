// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Spawner.generated.h"

UCLASS()
class SNAKEGAME_HOMEWORK_API ASpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		UBoxComponent* Box;

	UPROPERTY(EditAnywhere, Category = "Settings")
		TSubclassOf<AActor> SpawnClass;
	UPROPERTY(EditAnywhere, Category = "Settings")
		float SpawnPerTime;

	UPROPERTY(EditAnywhere, Category = "Settings")
		bool bIsOnce;

	void Spawn();
};