// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Classes/Components/StaticMeshComponent.h"
#include"SnakeBase.h"
#include "BonusSpeed.h"

// Sets default values
ABonusSpeed::ABonusSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BonusMesh"));
	RootComponent = MeshComponent;

}

// Called when the game starts or when spawned
void ABonusSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			
			if (Snake->MovementSpeed > 0.15f)
			{
				Snake->MovementSpeed -= 0.05f;
				Snake->SetActorTickInterval(Snake->MovementSpeed);
			}
			this->Destroy();
		}
	}

}

