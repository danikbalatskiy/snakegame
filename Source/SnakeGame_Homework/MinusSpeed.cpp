// Fill out your copyright notice in the Description page of Project Settings.

#include "MinusSpeed.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include"SnakeBase.h"


// Sets default values
AMinusSpeed::AMinusSpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BonusMesh"));
	RootComponent = MeshComponent;
}

// Called when the game starts or when spawned
void AMinusSpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMinusSpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMinusSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{

			if (Snake->MovementSpeed < 0.7f)
			{
				Snake->MovementSpeed += 0.1f;
				Snake->SetActorTickInterval(Snake->MovementSpeed);
			}
			this->Destroy();
		}
	}
}

